import { LatLngExpression, CircleMarker, CircleMarkerOptions } from "leaflet";
export class SemiCircleMarker extends CircleMarker {
  constructor(
    latlng: LatLngExpression,
    options: SemiCircleMarkerOptions | CircleMarkerOptions
  );
    /*
    * Sets the rotation angle value.
    */
    setRotationAngle(newAngle: number): void;

    /*
    * Sets the rotation origin value.
    */
    setRotationOrigin(newOrigin: string): void;

    /*
    * Sets the start angle value.
    */
    setStartAngle(degrees: number): void;

    /*
    * Sets the stop angle value.
    */
    setStopAngle(degrees: number): void;

    /*
    * Sets the direction according a given direction and an angle value.
    */
    setDirection(direction:number, degrees: number): void;

    /*
    * Gets the direction.
    */
    getDirection(): number;

    /*
    * Checks if is a SemiCircle
    */
    isSemicircle(): boolean;
}

interface SemiCircleMarkerOptions extends CircleMarkerOptions {
  
    /*
    * Rotation angle, in degrees, clockwise. Defaults to 0.
    */
    startAngle?: number;

    /*
    * Rotation angle, in degrees, clockwise. Defaults to 359.9999.
    */
    stopAngle?: number;

}