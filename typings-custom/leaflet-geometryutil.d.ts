import * as L from 'leaflet';
declare module 'leaflet-geometryutil' {
  export class GeometryUtil {
    static computeAngle(a: L.Point, b: L.Point): number;
    static closest(map: L.Map, layer: L.Layer, latlng: L.LatLng, vertices?: boolean): L.LatLng;
  }
}