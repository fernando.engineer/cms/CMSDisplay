# Combat Management System Simulator DISPLAY 

CMSDisplay is the frontend component providing UI for the Combat Management System Simulator/Game.<br>
Based on AngularJS and built using reactive programming and Leaflet map features.<br>
CMSDisplay is a Kafka subscriber off [TrackHandler](https://gitlab.com/fernando.engineer/cms/TrackHandler) CMS microservice component, and also do HTTP API REST requests to it.<p>

![CMSDisplay](images/CMSDisplay_2023-03-26_13.12.16.png)

## Components



### Map

Map Component is the main activity, provides the world map where everything happens.<br>
Track is the main entity of the CMS and is basically a ship (but can also be another kind of object travelling in the world, e.g a Missile WEAPON! So take care!)<br>
Through the map is possible visualize and select Tracks.<br>
For each Track is also possible visualize some of its interesting characteristcs for instance:

<table border="1">
    <tr>
        <td><img src="images/TrackView.png"></td>
        <td>

- The route history (represented by a dashed line behind it)
- The projected route (represented by a thin solid line starting from its bow)
- The projected route (represented by a thin solid line starting from its bow)
    - The size of the projected route is a consequence of the Track SoG (Speed Over Groud) and its default value represent 1 hour of navigation at that given speed.
- The Self-Noise produced, represented by a orange Circle Marker around the Track.
    - The noise is assumed isotropical and its range depends also from Track Speed, but also other parameters from Track specification (namely Self-Noise parameters)
    - The area represented by the self noise is the area where the Track is vulnerable to be detected by a passive Sonar (so don't speed up too much or you'll be discovered, which reminds us of our next feature..)
- The detection area of your passive Sonar, represented by a blue [Arc Marker](https://www.npmjs.com/package/leaflet-marker-arc) around the Track.
    - The passive Sonar azimuth and distance range are Track built-in parameter from Track specification and can be changed only if you replace your equipment (coming soon ;))
    - Passive Sonar characteristics can also be affected by the sea conditions (also coming soon ;))
The detection area of your active Radar, represented by a green [Arc Marker](https://www.npmjs.com/package/leaflet-marker-arc) around the Track.
    - The Radar bean azimuth, distance range and antenna speed rotation are also Track built-in parameters from Track specification.

        </td>
    </tr>
</table>



### Track-List


<table border="1">
    <tr>
    <td><img src="images/TrackList.png"></td>
    <td>

Components witch provides a list of existing Tracks on the CMS.<br>
The list can be minimized and when expanded present the Track Id and Name providing tracking selection capabilities.<br>
When [TrackHandler](https://gitlab.com/fernando.engineer/cms/TrackHandler) is on, its color is green, when the server is of, its changes to red.
    </td>
    </tr>
</table>


### TrackInfo


<table border="1">
    <tr>
    <td><img src="images/TrackInfo.png"></td>
    <td>

Component providing real time information about the selected Track, its Tactical Situation, Performance and Specification.<br>
Through this component is possible to control the The Track, if it belongs to you, including the following actions: 


- Self-Destruction: 
    - Clicking this button a delete HTTP API request will be sent to [TrackHandler](https://gitlab.com/fernando.engineer/cms/TrackHandler) and your ship will be destroyed.
- Kinematic Control: 
    - Using the (-) and (+) buttons around CoG (Course Over Ground) and SoG (Speed Over Ground) [TrackHandler](https://gitlab.com/fernando.engineer/cms/TrackHandler) will be requested to change its values.
    - Please be aware that as in real life, your maneuver is not automatic: The time your ship will take to meet the speeds and ordered courses also depends on constructive factors. (notably 'Tactical Performance' parameters, also displayed in the TrackInfo component).
    - While the Current CoG and SoG is not the ordered ones, the values transition is highlighed in red in the TrackInfo.
- Launch Weapon: 
    - Clicking it, your ship will launch a Missile (that is a new Track in the CMS) if your ship have available weapons.

The TrackInfo panel can be partial or fully minimized.<br>
When [TrackHandler](https://gitlab.com/fernando.engineer/cms/TrackHandler) is on, its color is blue, when the server is of, its changes to red.
    </td>
    </tr>
</table>



### Track-Alerts
<table border="1">
    <tr>
    <td><img src="images/TrackAlert.png"></td>
    <td>

Components witch provides a list of CMS Alerts.<br>
The list can be minimized and when expanded present the alerts sent by other CMS components (e.g [TrackHandler](https://gitlab.com/fernando.engineer/cms/TrackHandler)).<br>
When [TrackHandler](https://gitlab.com/fernando.engineer/cms/TrackHandler) is on, its color is yellow, when the server is of, its changes to red.
    </td>
    </tr>
</table>