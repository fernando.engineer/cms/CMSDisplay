export const environment = {
  production: true,
  scope: "production",
  apiWSURL: "wss://trackhandler.cms.lab.fernando.engineer/websocket",
  apiAISWSURL: "wss://trackhandler.cms.lab.fernando.engineer/aissocket",
  apiHTTPURL: "https://trackhandler.cms.lab.fernando.engineer",
  socket: `://${location.host}/api/socket`
};
