import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from './../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http'
import { KafkaState } from './state/kafka.state';
import { MapComponent } from './map/map.component';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { NgxsWebsocketPluginModule } from '@ngxs/websocket-plugin';
import { TrackListComponent } from './track-list/track-list.component';
import { TrackService } from './services/track.service';
import { TrackPopupService } from './services/trackpopup.service';
import { TrackinfoComponent } from './trackinfo/trackinfo.component';
import { TrackAlertsComponent } from './track-alerts/track-alerts.component';

@NgModule({
  declarations: [
    AppComponent,
    TrackListComponent,
    MapComponent,
    HeaderComponent,
    TrackinfoComponent,
    TrackAlertsComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxsModule.forRoot([KafkaState]), 
    NgxsWebsocketPluginModule.forRoot({url: environment.apiWSURL}),
    ReactiveFormsModule
  ],
  providers: [
    TrackService,
    TrackPopupService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
