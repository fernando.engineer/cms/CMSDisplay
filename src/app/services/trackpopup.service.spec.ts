import { TestBed } from '@angular/core/testing';

import { TrackPopupService } from './trackpopup.service';

describe('TrackpopupService', () => {
  let service: TrackPopupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrackPopupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
