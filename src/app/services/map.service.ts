import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Track } from '../classes/track';
import { TrackView } from '../classes/track-view';
import { TrackService } from './track.service';
import { LatLng } from 'leaflet';
import * as L from 'leaflet';
import { Store } from '@ngxs/store';
import { ConnectWebSocket } from '@ngxs/websocket-plugin';
import WebsocketHeartbeatJs from 'websocket-heartbeat-js';
import { AisService } from './ais.service';
import { AISTrackView } from '../classes/AISTrackView';
import { AISTrackInfo } from '../classes/aistrackinfo';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  ownShipTracksLayerGroup = L.layerGroup();
  aisTracksLayerGroup = L.layerGroup(); 
  static map: L.Map;
  mapDefaultZoom:number =4;
  mapCurrentZoom:number;
  //Tuple containing identifier and type of selected track.
  static selectedTrackId: [string,string] = ["-1","NONE"];
  

  static WEAPON_AUTOGUIDED_SENSIBILITY_AZIMUTH = 15;
  static WEAPON_AUTOGUIDED_GAIN_AZIMUTH = 3;
  static websocketHeartbeatJs:WebsocketHeartbeatJs = new WebsocketHeartbeatJs({url: environment.apiWSURL});
  static heartCheck = {
    previousState: true,
    timeout: 5000,
    timeoutObj: 0,
    reset: function(){
      clearTimeout(this.timeoutObj);
      this.start();
    },
    start: function(){
      this.timeoutObj = window.setTimeout(() =>{
        MapService.websocketHeartbeatJs.send("HeartBeat");
        }, this.timeout)
    }
  }

  constructor(private store:Store, private trackService:TrackService, private aisService:AisService) { 
    this.store.dispatch(new ConnectWebSocket())
    this.aisService.aisTracksInfoMap$.subscribe(data => this.getAISTrackViews());
  }

  initMap(): void {
    MapService.map = L.map('map', {
      center: [ 25, -0 ],
      zoomSnap: 0.25,
      zoomDelta: 1,
      wheelPxPerZoomLevel: 60,
      zoom: 3
    });
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 30,
      minZoom: 3,
      attribution: '<!--&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>-->'
    });
    tiles.addTo(MapService.map);
    MapService.map.addLayer(this.ownShipTracksLayerGroup);
    MapService.map.addLayer(this.aisTracksLayerGroup);

    MapService.map.on('click', (e) => {
      let coordinate:LatLng = new L.LatLng(e.latlng.lat, e.latlng.lng);
      MapService.map.panTo(coordinate);
      this.trackService.clickedCoordinate(coordinate);
      this.selectTrack("-1","NONE");
    });

    MapService.map.on('zoomend',(e) =>{
      this.mapCurrentZoom = MapService.map.getZoom();
      this.trackService.trackViewsMap.forEach((trackView: TrackView) => trackView.updateRadius());
      this.aisService.aisTrackViewsMap.forEach((aisTrackView: AISTrackView) => {
        if(typeof aisTrackView.marker != 'undefined'){
          aisTrackView.marker.setIcon(aisTrackView.getIcon(this.trackService.trackHandlerOn))        
        }
      });
      //TODO: Debug/Reimplement AISTrack radar
      //this.aisService.aisTrackViewsMap.forEach((aisTrackView: AISTrackView) => aisTrackView.updateRadius());
    });

    MapService.websocketHeartbeatJs.onopen = () => {
      if(!MapService.heartCheck.previousState){
       console.log("SOCKET REOPENED");
       while(!window.confirm("TrackHandler Connection Restablished. Push OK to Reload.")){
        // Waiting for confirmation.
       }
       this.resetTracks();
       window.location.reload();
      }
      this.trackService.trackHandlerOn = true;
      MapService.heartCheck.start();
     };
     MapService.websocketHeartbeatJs.onmessage = () => {
       console.log("HEARTBEAT RECEIVED");
       this.trackService.trackHandlerOn = true;
       MapService.heartCheck.reset();
     }
     MapService.websocketHeartbeatJs.onerror = () => {
       console.log("SOCKET ERROR, RECONNECTING..");
       this.trackService.trackHandlerOn = false;
       MapService.heartCheck.previousState = false;
       if(!this.trackService.trackHandlerOn){
        this.trackService.trackViewsMap.forEach((trackView: TrackView) => {
          trackView.marker.setIcon(trackView.getIcon(false));
          trackView.noiseMarker.setRadius(0);
          trackView.detectionMarker.setRadius(0);
          if(trackView.track.sensorTrackType == "OWNSHIP"){
            trackView.radarDetectionMarker.setRadius(0);
          }
          if(trackView.track.sensorTrackType == "WEAPON"){
            trackView.amplifiedDetectionMarker.setRadius(0);
          }
        });
      } 
       MapService.websocketHeartbeatJs.reconnect();
     }
  }
  public resetTracks(){
    this.trackService.tracksMapObservable.next(new Map<string, Track>());
    this.trackService.tracksViewMapObservable.next(new Map<string,TrackView>());
    this.getTrackViews();
  }

  public getTrackViews(){
    try {
      if (typeof this.trackService.trackViewsMap !== 'undefined'){
        // I - Check if we received all tracks that we already had. Remove the trackView if not.
        this.purgeSystemTracks();
        // II - Now we remove the deleted tracks, we process the other tracks (shall already exist or be created)
        this.trackService.tracksMap.forEach((track: Track, trackId: string) => {      
          if(this.trackService.trackViewsMap.has(trackId)){
            let trackView = this.trackService.trackViewsMap.get(trackId);
            if(typeof trackView != 'undefined'){
              this.updateTrackView(trackView, track);
            }
          }
          else{
            this.trackService.trackViewsMap.set(trackId,this.createNewTrack(track));
            this.trackService.tracksViewMapObservable.next(this.trackService.trackViewsMap);
          }
        });
      } 
    }
    catch (e: unknown) {
      if (typeof e === "string") {
        console.log(e.toUpperCase()) 
      } 
      else if (e instanceof Error) {
        console.log(e.message) 
      }
    }
  }

  public getAISTrackViews(){
    if (typeof this.aisService.aisTracksInfoMap !== 'undefined'){
      // I - Check if we received all tracks that we already had. Remove the trackView if not.
      this.purgeAISTracks();
      // II - Now we remove the deleted tracks, we process the other tracks (shall already exist or be created)
      
      this.aisService.aisTracksInfoMap.forEach((aisTrackInfo: AISTrackInfo, mmsi: number) => {      
        if(this.aisService.aisTrackViewsMap.has(mmsi)){
          let aisTrackView = this.aisService.aisTrackViewsMap.get(mmsi);
          if(typeof aisTrackView != 'undefined'){
            this.updateAISTrackView(aisTrackView, aisTrackInfo);
          }

        }
        else{
          this.aisService.aisTrackViewsMap.set(mmsi,this.createNewAISTrack(aisTrackInfo));
          this.aisService.aisTracksViewMapObservable.next(this.aisService.aisTrackViewsMap);
        }
      });
      
    } 
  }

  private purgeSystemTracks() {
    this.trackService.trackViewsMap.forEach((trackView: TrackView, trackId: string) => {
    // First try to locate a existing track in the this.trackService.tracksMap.
    if (!this.trackService.tracksMap.has(trackId)) {
      //If the previous existing track is not present, we shall remove it from map
      this.ownShipTracksLayerGroup.removeLayer(trackView.marker);
      this.ownShipTracksLayerGroup.removeLayer(trackView.lineHistory);
      this.ownShipTracksLayerGroup.removeLayer(trackView.lineProjection);
      trackView.detectionMarker.remove();
      trackView.noiseMarker.remove();
      // 1st Spike for Radar Detection: Only for OWNSHIPS    
      if(trackView.track.sensorTrackType == "OWNSHIP"){
        trackView.radarDetectionMarker.remove();
      }
      if(trackView.track.sensorTrackType == "WEAPON"){
        trackView.amplifiedDetectionMarker.remove();
      }
      // If the track lost were selected, deselect it
      if(trackView.selected){
        MapService.selectedTrackId = ["-1","NONE"];
      }
      // And update the Map
      this.trackService.trackViewsMap.delete(trackId);
      this.trackService.tracksViewMapObservable.next(this.trackService.trackViewsMap);
    }
  });
  }

  private purgeAISTracks() {
    this.aisService.aisTrackViewsMap.forEach((aisTrackInfoView: AISTrackView, mmsi:number ) => {
      // First try to locate a existing AISTrackInfo in the aisTracksInfoMap
      if (!this.aisService.aisTracksInfoMap.has(mmsi)) {
        //If the previous existing track is not present, we shall remove it from map
        this.aisTracksLayerGroup.removeLayer(aisTrackInfoView.marker);
        this.aisTracksLayerGroup.removeLayer(aisTrackInfoView.lineHistory);
        // 1st Spike for Radar Detection for AISTracks  
        aisTrackInfoView.radarDetectionMarker.remove();
        
        // If the track lost were selected, deselect it
        if(aisTrackInfoView.selected){
          this.aisService.selectTrack(-1, "NONE");
          MapService.selectedTrackId = ["-1","NONE"];
        }
        // And update the Map
        this.aisService.aisTrackViewsMap.delete(mmsi);
        this.aisService.aisTracksViewMapObservable.next(this.aisService.aisTrackViewsMap);
      }
    });
  }

  private createNewTrack(receivedTrack: Track): TrackView{
    // It is a new Track
    let newTrackView = new TrackView(receivedTrack);
    newTrackView.marker.on("click",  (event) => {
      //TODO: Re-enable new action on let clickedMarker: L.Marker = event.target;
      this.onClickMarker(newTrackView);
    });
    newTrackView.marker.on("drag",  (event) => {
      var marker = event.target;
      var position = marker.getLatLng();
      newTrackView.marker.setLatLng(position);
    });
    this.ownShipTracksLayerGroup.addLayer(newTrackView.marker);
    this.ownShipTracksLayerGroup.addLayer(newTrackView.lineProjection);
    return newTrackView;
  }

  createNewAISTrack(aisTrackInfo: AISTrackInfo): AISTrackView {
    let aisTrackView = new AISTrackView(aisTrackInfo);
    if(typeof aisTrackView.marker != 'undefined'){
      aisTrackView.marker.on("click",  () => {
        this.onClickAISMarker(aisTrackView);
      });
      this.aisTracksLayerGroup.addLayer(aisTrackView.marker);
    }
    return aisTrackView;
  }


  private onClickMarker(newTrackView: TrackView) {
    this.selectTrack(newTrackView.track.trackId, "SYSTEMTRACK");
  }

  private onClickAISMarker(aisTrackView: AISTrackView) {
    this.selectTrack(aisTrackView.aisTrackInfo.aisTrack.mmsi, "AIS");
  }

  private updateTrackView(trackView: TrackView, receivedTrack: Track) {
    trackView.update(receivedTrack);
    // If updated track is the current selection, fire its update.
    if(trackView.selected){
      this.trackService.selectedTracksObservable.next(trackView);
    }
  }

  private updateAISTrackView(aisTrackView: AISTrackView, receivedTrack: AISTrackInfo) {
    aisTrackView.update(receivedTrack);
    // If updated track is the current selection, fire its update.
    if(aisTrackView.selected){
      this.aisService.aisTracksInfoSelectedObservable.next(aisTrackView.aisTrackInfo); 
    }
  }

  selectTrack(trackId:any, sensorTrackType:string){
    switch(sensorTrackType){
      case 'SYSTEMTRACK':
        this.aisService.selectTrack(trackId, "SYSTEMTRACK");
        this.selectSystemTrack(trackId, "SYSTEMTRACK");
      break;
      case 'AIS':
        this.selectSystemTrack(trackId, "AIS");
        this.aisService.selectTrack(trackId, "AIS");
        break;
      case 'NONE':
        this.selectSystemTrack('-1', "NONE");
        this.aisService.selectTrack(-1, "NONE");
        MapService.selectedTrackId = ["-1","NONE"];
        break;
      default:
      break;    
    }
  }

  selectSystemTrack(trackId:string, trackType:string){
    // I - If the track was already selected, nothing to do just zoom
    if(MapService.selectedTrackId[0] == trackId && trackType == "SYSTEMTRACK"){
      let selectedTrack = this.trackService.tracksMap.get(trackId);
      if(typeof selectedTrack !== 'undefined'){
        MapService.map.setView([selectedTrack.latitude, selectedTrack.longitude],MapService.map.getZoom()+1,)
        return;
      }
      // Undefined Selected Track fire deselection (Shall not occur)
      console.error("Selected SystemTrack" + trackId + " not found in tracksMap: Shall not happens.")
      this.trackService.fireFakeTrackView();
      return;
    }

    // II - For all tracks reset previous selection.
    this.trackService.trackViewsMap.forEach((localtrackView: TrackView) => { 
      if(typeof localtrackView.lineHistory !== 'undefined' && localtrackView.lineHistory != null){
        this.ownShipTracksLayerGroup.removeLayer(localtrackView.lineHistory);
        localtrackView.lineHistory = null;
      }
      localtrackView.selected = false
      localtrackView.marker.setIcon(localtrackView.getIcon(true));
    });

    // None Systemtrack was selected, just fire deselection event and return.
    this.trackService.fireFakeTrackView();
    if(trackType != "SYSTEMTRACK"){
      return;
    }

    // III - Apply Selection over selected Track:
    let selectedTrackView = this.trackService.trackViewsMap.get(trackId);
    if(typeof selectedTrackView !== 'undefined'){ 
      // Center map in the selected track with the current zoom
      MapService.map.setView([selectedTrackView.track.latitude, selectedTrackView.track.longitude],MapService.map.getZoom(),)
      // Mark it as the new selected track
      MapService.selectedTrackId = [trackId, "SYSTEMTRACK"];
      selectedTrackView.selected = true;
      this.trackService.selectedTracksObservable.next(selectedTrackView);
      selectedTrackView.marker.setIcon(selectedTrackView.getIcon(true));

      // IV - History Cold Start, retrieving previous history when track is selected.
      this.trackService.getTrackHistory(selectedTrackView.track.trackId).subscribe({
        next: (trackTacticalUpdates) => this.buildTrackHistoryPolyline(selectedTrackView, trackTacticalUpdates) ,
        error: (e) => console.log(e),
        complete: () => console.log("["+ trackId +"]" + "- Track history received.") 
      });       
    }else {
      // None track was selected, just fire deselection event. (Shall not occur)
      console.error("Selected SystemTrack" + trackId + " not found in tracksMap: Shall not happens.")
      this.trackService.fireFakeTrackView();
    }
  }

  private buildTrackHistoryPolyline(trackView: TrackView | undefined, data: any):void {
    if(typeof trackView !== 'undefined' && typeof data !== 'undefined'){
      let warmHistorylatlongs = [];
      for (let j = data.length ; j > 0; j--) {
        warmHistorylatlongs.push(new L.LatLng(data[j-1].latitude, data[j-1].longitude));
      }
      if(trackView.track.sensorTrackType == "WEAPON"){
        trackView.lineHistory = L.polyline(warmHistorylatlongs, { color: 'red', dashArray: '1,4,10,4', stroke: true, fill: false, weight: 2, opacity: 0.7 });
      } 
      if(trackView.track.sensorTrackType == "OWNSHIP"){
        trackView.lineHistory = L.polyline(warmHistorylatlongs, { color: '#175533', dashArray: '3,4,8,4', stroke: true, fill: false, weight: 1, opacity: 0.9 });
      }
      this.ownShipTracksLayerGroup.addLayer(trackView.lineHistory);
    }
  }

  public static getRadiusNoise(track:Track, threshold:number){
    //TODO: Implement realistic Absorption of sound in seawater formula according Enviroment System Component
    //TODO: For now, using the default alpha = 0.2 dB/km (0.0002 db/m)
    //TODO: Counterpart: Implement realistic passive sonar model formula : for now, using SNR (decibels) = SL – 2TL + TS – NL (basic sonar equation)
    //TODO: For basic sonar equation: SL = source level (dbNoise), TL (transmission loss: based on default absortion of sound and alpha above)
    //TODO: For now, TS (Target strengh) and Noise Level (NL) are ignored, reducing our Sonar formula to : SNR(db) = SL - 2TL.
    //TODO: The amount of SNR exceeds the detection threshold (DT) is called signal excess (SE) : SE(db) = SBR - DT. If SE > 0 we have a detection.
    //TODO: Since we are ignoring Noise Level for now, lets just assume SE(db) when SNR > 1.
    //TODO: In this context, the radius of circle marker (representing the point where the ship can be still detected) is just the value where SL - 2TL = 1
    //TODO: Therefore this.dbNoise - alpha * radius = 1  -> radius(km) = this.dbNoise/alpha
    var currentPos = new LatLng(track.latitude,track.longitude);
    var maxDistance = (track.dBNoise - threshold) /0.0002;
    var radiusLatLng = MapService.destination(currentPos, 0, maxDistance);
    var currentLatLongLeaflet = new L.LatLng(track.latitude, track.longitude);
    var currentZoom = MapService.map.getZoom();
    var projectedcurrentPos = MapService.map.project(currentLatLongLeaflet,currentZoom);
    var projectedRadiusPos = MapService.map.project(new L.LatLng(radiusLatLng.lat, radiusLatLng.lng),currentZoom);
    let radius:number = MapService.getDistance(projectedcurrentPos.x, projectedcurrentPos.y, projectedRadiusPos.x, projectedRadiusPos.y);
    if(typeof radius === 'undefined'){
      radius = 0;
    }
    return radius;
}

  public static getRadiusSensorDetection(latitude:number, longitude:number, detectionRange:number){
    let currentPos = new L.LatLng(latitude, longitude);
    // Angle = 0 : Dont mind the angle we are just computing radius.
    //TODO: Adjust gain factor directly at backend side
    let amplifiedrange = detectionRange;
    let radiusLatLng  = MapService.destination(currentPos, 0, amplifiedrange);
    var currentZoom = MapService.map.getZoom();
    var projectedcurrentPos = MapService.map.project(currentPos,currentZoom);
    var projectedRadiusPos = MapService.map.project(new L.LatLng(radiusLatLng.lat, radiusLatLng.lng),currentZoom);
    return MapService.getDistance(projectedcurrentPos.x, projectedcurrentPos.y, projectedRadiusPos.x, projectedRadiusPos.y); 
  }


      /**
      Returns the point that is a distance and heading away from
      the given origin point.
      @param {L.LatLng} latlng: origin point
      @param {float} heading: heading in degrees, clockwise from 0 degrees north.
      @param {float} distance: distance in meters
      @returns {L.latLng} the destination point.
      Many thanks to Chris Veness at http://www.movable-type.co.uk/scripts/latlong.html
      for a great reference and examples.
    */
      private static destination(latlng:LatLng, heading:number, distance:number):L.LatLng {
        heading = (heading + 360) % 360;
        var rad = Math.PI / 180,
        radInv = 180 / Math.PI,
        R = 6378137, // approximation of Earth's radius
        lon1 = latlng.lng * rad,
        lat1 = latlng.lat * rad,
        rheading = heading * rad,
        sinLat1 = Math.sin(lat1),
        cosLat1 = Math.cos(lat1),
        cosDistR = Math.cos(distance / R),
        sinDistR = Math.sin(distance / R),
        lat2 = Math.asin(sinLat1 * cosDistR + cosLat1 *
          sinDistR * Math.cos(rheading)),
        lon2 = lon1 + Math.atan2(Math.sin(rheading) * sinDistR *
          cosLat1, cosDistR - sinLat1 * Math.sin(lat2));
        lon2 = lon2 * radInv;
        lon2 = lon2 > 180 ? lon2 - 360 : lon2 < -180 ? lon2 + 360 : lon2;
        return L.latLng([lat2 * radInv, lon2]);
      }
    
    
      private static getDistance(x1:number, y1:number, x2:number, y2:number){
        let y = x2 - x1;
        let x = y2 - y1;
        return Math.round(Math.sqrt(x * x + y * y));
      }
}
