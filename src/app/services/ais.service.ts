import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { plainToInstance } from 'class-transformer';
import { Subject } from 'rxjs';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import { AISTrack } from '../classes/aistrack';
import { AISTrackInfo } from '../classes/aistrackinfo';
import { AISTrackView } from '../classes/AISTrackView';
import { MapService } from './map.service';

@Injectable({
  providedIn: 'root'
})
export class AisService {

    aisWebSocket$: WebSocketSubject<any> = webSocket(environment.apiAISWSURL);
    public aisTracksInfoMapObservable = new Subject<Map<number,AISTrackInfo>>();
    public aisTracksInfoMap$ = this.aisTracksInfoMapObservable.asObservable();
    aisTracksInfoMap = new Map<number,AISTrackInfo>();
    aisTrackViewsMap:Map<number,AISTrackView> = new Map<number,AISTrackView>();

    public aisTracksViewMapObservable = new Subject<Map<number,AISTrackView>>();
    public aisTracksViewMap$ = this.aisTracksViewMapObservable.asObservable();

    public aisTracksInfoSelectedObservable:Subject<AISTrackInfo> = new Subject<AISTrackInfo>();
    public aisTracksInfoSelected$= this.aisTracksInfoSelectedObservable.asObservable();
    private selectedAISTrackInfo: AISTrackInfo | undefined;

  constructor() {
    this.aisWebSocket$.subscribe({
      next: (aisTrackInfo) =>  this.processAISTrackInfoMessage(aisTrackInfo) ,
      error: (e) => console.log(e),
      complete: () => console.log("AIS socket closed.") 
    });

  }
  processAISTrackInfoMessage(aisTrackInfo: AISTrackInfo) {
    let aisTrack:AISTrack;
    aisTrack = plainToInstance(AISTrack,aisTrackInfo.aisTrack);
    if(this.selectedAISTrackInfo?.aisTrack.mmsi == aisTrack.mmsi){
      aisTrackInfo.selected = true;
      this.aisTracksInfoSelectedObservable.next(aisTrackInfo);
    }
    this.aisTracksInfoMap.set(aisTrack.mmsi,aisTrackInfo);
    this.aisTracksInfoMapObservable.next(this.aisTracksInfoMap);
  }

  public selectTrack(mmsi:number, trackType:string){
    // I - If the track was already selected, nothing to do just zoom
    if(MapService.selectedTrackId[0] ==  mmsi.toString() && trackType == "AIS"){
      let selectedTrack = this.aisTrackViewsMap.get(mmsi);
      if(typeof selectedTrack !== 'undefined' && typeof selectedTrack.marker != 'undefined'){
        MapService.map.setView([selectedTrack.aisTrackInfo.aisTrack.latitude, selectedTrack.aisTrackInfo.aisTrack.longitude],MapService.map.getZoom()+1,)
        return;
      }
      return;
    } 

    // II - For all tracks reset previous selection
    this.aisTrackViewsMap.forEach((aisTrackView: AISTrackView) => {
      aisTrackView.aisTrackInfo.selected = false;
      if(typeof aisTrackView.marker != 'undefined'){
        aisTrackView.marker.setIcon(aisTrackView.getIcon(true));
      }    
    });

    // III - None AISTrack was selected, fire a fake AISTrack event to inform deselection.
      if(trackType != "AIS"){
        let aisTrack:AISTrack = new AISTrack();
        aisTrack.mmsi = -1;
        // Just fire global deselection if another track type was not selected instead.
        this.aisTracksInfoSelectedObservable.next(new AISTrackInfo(aisTrack,[]));
      return;
    }

    // IV - Find, Select and Fire event for new selected AISTrack.
    this.selectedAISTrackInfo = this.aisTracksInfoMap.get(mmsi);
    if(typeof this.selectedAISTrackInfo != 'undefined'){
      this.selectedAISTrackInfo.selected = true;
      MapService.selectedTrackId = [mmsi.toString(),"AIS"];
      let aisTrackView = this.aisTrackViewsMap.get(mmsi);
      if(typeof aisTrackView != 'undefined' && typeof aisTrackView.marker != 'undefined'){
        aisTrackView.marker.setIcon(aisTrackView.getIcon(true));
        MapService.map.setView([this.selectedAISTrackInfo.aisTrack.latitude,
          this.selectedAISTrackInfo.aisTrack.longitude],MapService.map.getZoom(),)
      }
      this.aisTracksInfoMapObservable.next(this.aisTracksInfoMap);
      this.aisTracksInfoSelectedObservable.next(this.selectedAISTrackInfo);
    }
  }


    /* TODO: Use to Display specific AISMessages
    let aisTrackMessages:AISMessage[] = [];
    aisTrackInfo.aisTrackMessages.forEach( (aisTrackMessage: { messageType: any; }) => {
      let aisMessage:AISMessage | undefined = undefined;
      switch (aisTrackMessage.messageType){
        case 'SHIP_AND_VOYAGE_RELATED_DATA' :
          aisMessage = plainToInstance(PositionReportClassAScheduled,aisTrackMessage); 
          aisTrackMessages?.push(aisMessage);
        break;
        case 'POSITION_REPORT_CLASS_A_SCHEDULED':
           aisMessage = plainToInstance(PositionReportClassAScheduled,aisTrackMessage);
           aisTrackMessages?.push(aisMessage);
        break;
        default:
          let msgNotImplemented:string = aisTrackMessage.messageType + " not implemented";
      }
      
    });
    */

}
