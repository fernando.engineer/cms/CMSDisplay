import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom, Observable, Subject } from 'rxjs';
import { Track } from '../classes/track';
import { TrackView } from '../classes/track-view';
import { AISTrack } from '../classes/aistrack';
import { TrackSpecification } from '../classes/trackspecification';
import { TrackTacticalUpdate } from '../classes/tracktacticalupdate';
import { LatLng } from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class TrackService {

  public tracksMapObservable = new Subject<Map<string,Track>>();
  public tracksMap$ = this.tracksMapObservable.asObservable(); 
  public tracksViewMapObservable = new Subject<Map<string,TrackView>>();
  public tracksViewMap$ = this.tracksViewMapObservable.asObservable();
  public selectedTracksObservable = new Subject<TrackView>();
  public selectedTrack$ = this.selectedTracksObservable.asObservable();

  trackHandlerOn: boolean;
  trackSpecificationMap: Map<string,TrackSpecification> = new Map();
  tracksMap:Map<string,Track> = new Map<string, Track>();
  trackUpdates = new Map<string, TrackTacticalUpdate[]>();
  trackViewsMap:Map<string,TrackView> = new Map<string,TrackView>();
  interval: any;
  selectedCoordinate:LatLng | null = null;
  private urlTrackHandler = environment.apiHTTPURL;
  private urlTracks = this.urlTrackHandler + "/track";
  private urlAISTracks = this.urlTrackHandler + "/aistracks";
  private postTracks = this.urlTracks;
  public cmdStatus:string;

  
  constructor(private httpClient: HttpClient) { 
    this.tracksMapObservable.next(new Map<string, Track>());
  }

  //Fake track used to fire deselection event.
  public fireFakeTrackView():void {
    let fakeTrack = new Track();
    fakeTrack.trackId = "-1";
    let fakeTrackView = new TrackView(fakeTrack);
    this.selectedTracksObservable.next(fakeTrackView);
  }

  async postTrackFormData(formData:any){ 
    return await lastValueFrom(this.httpClient.post(this.postTracks, formData));
  } 

  getTrackHistory(trackId: string): Observable<TrackTacticalUpdate[]>{
    let urlTrackHistory = `${this.urlTracks}/${trackId}/history`;
    return this.httpClient.get<TrackTacticalUpdate[]>(`${urlTrackHistory}`);
  }

  getAISTracksList(): Observable<AISTrack[]>{
    return this.httpClient.get<AISTrack[]>(`${this.urlAISTracks}`);
  }

  deleteTrack(trackId: string) {
    let urlTrackDelete = `${this.urlTracks}/${trackId}`;
    this.httpClient.delete(`${urlTrackDelete}`).subscribe();
  } 

  launchWeapon(trackId: string){
    let urlTrackFire = `${this.urlTracks}/${trackId}/fire`;
    this.httpClient.get<Track>(`${urlTrackFire}`).subscribe();
  }

  orderCoG(trackId: string, orderedCoG:number){
    let urlOrderCoG = `${this.urlTracks}/${trackId}/cog/${orderedCoG}`;
    this.httpClient.get<[boolean]>(`${urlOrderCoG}`).subscribe();
  }

  orderSoG(trackId: string, orderedSoG:number){
    let urlOrderSoG = `${this.urlTracks}/${trackId}/sog/${orderedSoG}`;
    this.httpClient.get<[boolean]>(`${urlOrderSoG}`).subscribe();
  }

  clickedCoordinate(coordinate: any) {
    this.selectedCoordinate = coordinate;
  }

}
