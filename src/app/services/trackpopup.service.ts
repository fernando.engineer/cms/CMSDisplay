import { Injectable } from '@angular/core';
import { Track } from '../classes/track';

@Injectable({
  providedIn: 'root'
})
export class TrackPopupService {

  makeTrackPopup(track: Track): string { 
    return `` +
      `<div>Name: ${ track.name }</div>` +
      `<div>Lat: ${ track.latitude }</div>` +
      `<div>Long: ${ track.longitude }</div>`
  }

}
