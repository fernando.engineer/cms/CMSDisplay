import { Injectable } from '@angular/core';
import { Select } from '@ngxs/store';
import { plainToInstance } from 'class-transformer';
import { Observable } from 'rxjs';
import { AISMessage } from '../classes/aismessages/AISMessage';
import { AISTrackInfo } from '../classes/aistrackinfo';
import { Track } from '../classes/track';
import { TrackAlert } from '../classes/trackalert';
import { TrackMessage } from '../classes/trackmessage';
import { TrackSpecification } from '../classes/trackspecification';
import { KafkaState } from '../state/kafka.state';
import { AisService } from './ais.service';
import { MapService } from './map.service';
import { TrackService } from './track.service';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {
  
  @Select(KafkaState.receiveMessages)
  kafkaMessages$: Observable<TrackMessage[]>

  alertsList:TrackAlert[] = [];
  aisMessageList:AISMessage[] = [];

  constructor(public mapService: MapService, public trackService:TrackService, private aisService:AisService) { 
    this.kafkaMessages$.subscribe(
      data => {
        data.map(trackMessage => this.processTrackMessage(trackMessage))
      }
    )
  }
  
  public alertChecked(trackalert:TrackAlert){
    trackalert.checked = true;
  }

  processTrackMessage(trackMessage: TrackMessage): any {
    if(typeof trackMessage.topic != 'undefined'){
    //WORKAROUND: Filtering env dependent attributes. TODO: Migrate to profiles.
      let topic = trackMessage.topic.replace(/LOCAL_/gi, ""); 
      topic = topic.replace(/TEST_/gi, ""); 
      switch(topic){
        case 'AISMESSAGE_TOPIC':
          let strMessage:string = trackMessage.message;
          let aisTrackInfo:AISTrackInfo | undefined = undefined;
          aisTrackInfo = plainToInstance(AISTrackInfo,strMessage);
          this.aisService.processAISTrackInfoMessage(aisTrackInfo);
        break;
        case 'TACTICAL_ALERT_TOPIC':
          this.alertsList.unshift(new TrackAlert(trackMessage.timestamp,trackMessage.trackId,trackMessage.message));
        break;
        case 'TRACKDAO_TOPIC':
          let receivedTracksMap = new Map<string,Track>();
          let trackArray = [];
          trackArray = trackMessage.message;
          trackArray.forEach((element: any) => {
            let track = plainToInstance(Track,element)
            if(this.trackService.trackSpecificationMap.has(track.trackId)){
              track.specification = this.trackService.trackSpecificationMap.get(track.trackId)
            }
            receivedTracksMap.set(track.trackId, track);
          });
          this.trackService.tracksMapObservable.next(receivedTracksMap);
          this.trackService.tracksMap = receivedTracksMap;
          this.mapService.getTrackViews();
        break;
        case 'TRACKDAO_PERFORMANCE_TOPIC':
          let receivedSpecsMap = new Map<string,TrackSpecification>();
          let specArray = [];
          specArray = trackMessage.message;
          specArray.forEach((element: any) => {
            let trackSpec = plainToInstance(TrackSpecification,element)
            receivedSpecsMap.set(trackSpec.trackId, trackSpec);
            let track = this.trackService.tracksMap.get(trackSpec.trackId);
            if(typeof track !== 'undefined'){
              track.specification = trackSpec;
            }
          });
          this.trackService.trackSpecificationMap = receivedSpecsMap;
        break;
      }
    }
  }

}
