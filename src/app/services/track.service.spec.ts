import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { TrackService } from './track.service';


let httpServiceSpy: jasmine.SpyObj<HttpClient>;


describe('TrackService', () => {
  let trackService: TrackService;

  beforeEach(() => {
    const spyHttpClient = jasmine.createSpyObj('HttpClient', ['getValue']);

    TestBed.configureTestingModule({
      providers: [
        TrackService,
        { provide: HttpClient, useValue: spyHttpClient }
      ]
    });
    trackService = TestBed.inject(TrackService);
    httpServiceSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(trackService).toBeTruthy();
  });
});
