import { Component, AfterViewInit } from '@angular/core';
import { Track } from '../classes/track';
import { MapService } from '../services/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  selectedCCoordinate:any = null;
  localTracksMap:Map<string,Track> = new Map<string, Track>();

  constructor(private mapService: MapService) { }

  ngAfterViewInit(): void {
    this.mapService.initMap();

  /*  TODO: Explore new features from clicked coordinates on the map 
    this.mapService.map.on('click', (e) => {
      let coordinate:LatLng = new L.LatLng(e.latlng.lat, e.latlng.lng);
      this.mapService.map.panTo(coordinate);
      this.trackService.clickedCoordinate(coordinate);
      this.mapService.selectTrack(null);
      if(this.selectedCCoordinate!=null){
        this.selectedCCoordinate.remove();
      }
      this.selectedCCoordinate = L.circle([e.latlng.lat,e.latlng.lng], {radius: 15}).addTo(this.mapService.map);
    var xlng = 0.000256;
    var xlat = 0.000200;
      var c = L.circle([e.latlng.lat,e.latlng.lng], {radius: 15}).addTo(this.trackService.map);
      L.polygon([
        [e.latlng.lat-xlat,e.latlng.lng-xlng],
        [e.latlng.lat+xlat,e.latlng.lng-xlng],
        [e.latlng.lat-xlat,e.latlng.lng+xlng],
        [e.latlng.lat+xlat,e.latlng.lng+xlng],
      ]).addTo(this.trackService.map);
        L.polyline([
        [e.latlng.lat,e.latlng.lng-xlng],
        [e.latlng.lat,e.latlng.lng+xlng]
      ]).addTo(this.trackService.map); 
    });
*/
  }

}
