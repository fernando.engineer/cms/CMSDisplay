import { Track } from "./track";
import * as L from 'leaflet';
import * as R from 'leaflet-marker-rotation';
import { MapService } from "../services/map.service";
import { ArcMarker } from "leaflet-marker-arc";

export class TrackView {
    track: Track;
    selected: boolean = false;
    marker: R.RotatedMarker;
    lineHistory: any;
    lineProjection: any;
    periodProjection: number = 3600;
    textProjection: string = "1h";
    noiseMarker: L.CircleMarker;
    detectionMarker: any
    radarStarted: boolean = false;
    sonarStarted:boolean = false;
    radarDetectionMarker: any
    amplifiedDetectionMarker: any; 
    shipOwnIcon = L.icon({ iconUrl: 'assets/ownship.png', iconAnchor: [15, 5]});
    shipOwnIconTiny = L.icon({ iconUrl: 'assets/ownship_tiny.png', iconAnchor: [7, 2]});
    shipOwnIconNofuel = L.icon({ iconUrl: 'assets/ownship-nofuel.png', iconAnchor: [15, 5]});
    shipOwnIconOffline = L.icon({ iconUrl: 'assets/ownship-offline.png', iconAnchor: [15, 5]});
    shipOwnIconSelected = L.icon({ iconUrl: 'assets/ownship-selected.png', iconAnchor: [15, 5]});
    shipOwnIconTinySelected = L.icon({ iconUrl: 'assets/ownship_tiny_selected.png', iconAnchor: [7, 2]});
    shipOwnIconSelectedNoFuel = L.icon({ iconUrl: 'assets/ownship-selected-nofuel.png', iconAnchor: [15, 5]});
    shipOwnIconTargeted = L.icon({ iconUrl: 'assets/ownship-targeted.png', iconAnchor: [15, 5]});
    shipOwnIconTargetedSelected = L.icon({ iconUrl: 'assets/ownship-targeted-selected.png', iconAnchor: [15, 5]});
    WeaponIcon = L.icon({iconUrl: 'assets/weapon.png',iconAnchor: [8, 3]});
    WeaponIconNoFuel = L.icon({iconUrl: 'assets/weapon-nofuel.png',iconAnchor: [8, 3]});
    WeaponIconOffline = L.icon({iconUrl: 'assets/weapon-offline.png',iconAnchor: [8, 3]});
    WeaponIconSelected = L.icon({iconUrl: 'assets/weapon-selected.png',iconAnchor: [8, 3]});
    WeaponIconSelectedNoFuel = L.icon({iconUrl: 'assets/weapon-selected-nofuel.png',iconAnchor: [8, 3]});
   
       /** Array Index Definition:
     * [0] - Icon Offline
     * [1] - (0,0,0,0) - (Not Targeted, No Fuel Empty, Not Tiny , Not Selected)
     * [2] - (0,0,0,1) - (Not Targeted, No Fuel Empty, Not Tiny , Selected)    
     * [3] - (0,0,1,0) - (Not Targeted, No Fuel Empty, Tiny , Not Selected)
     * [4] - (0,0,1,1) - (Not Targeted, No Fuel Empty, Tiny , Selected)   
     * [5] - (0,1,0,0) - (Not Targeted, Fuel Empty, Not Tiny , Not Selected)
     * [6] - (0,1,0,1) - (Not Targeted, Fuel Empty, Not Tiny , Selected)    
     * [7] - (0,1,1,0) - (Not Targeted, Fuel Empty, Tiny , Not Selected)
     * [8] - (0,1,1,1) - (Not Targeted, Fuel Empty, Tiny , Selected)  
     * [9] - (1,0,0,0) - (Targeted, No Fuel Empty, Not Tiny , Not Selected)
     * [10] - (1,0,0,1) - (Targeted, No Fuel Empty, Not Tiny , Selected)    
     * [11] - (1,0,1,0) - (Targeted, No Fuel Empty, Tiny , Not Selected)
     * [12] - (1,0,1,1) - (Targeted, No Fuel Empty, Tiny , Selected)   
     * [13] - (1,1,0,0) - (Targeted, Fuel Empty, Not Tiny , Not Selected)
     * [14] - (1,1,0,1) - (Targeted, Fuel Empty, Not Tiny , Selected)    
     * [15] - (1,1,1,0) - (Targeted, Fuel Empty, Tiny , Not Selected)
     * [16] - (1,1,1,1) - (Targeted, Fuel Empty, Tiny , Selected)  
     */ 

       iconsMultiMap: Map<string, Array<L.Icon>> = new Map<string, Array<L.Icon>>(
        [
            ["OWNSHIP", 
                [
                    this.shipOwnIconOffline, 
                    this.shipOwnIcon,this.shipOwnIconSelected,this.shipOwnIconTiny,this.shipOwnIconTinySelected,
                    //TODO: Create no fuel Tiny versions
                    this.shipOwnIconNofuel,this.shipOwnIconSelectedNoFuel,this.shipOwnIconNofuel,this.shipOwnIconSelectedNoFuel,
                   //TODO: Create targeted with Fuel Normal and Tiny versions
                    this.shipOwnIconTargeted,this.shipOwnIconTargetedSelected,this.shipOwnIconTargeted,this.shipOwnIconTargetedSelected,
                    //TODO: Create targeted Tiny Versions
                    this.shipOwnIconTargeted,this.shipOwnIconTargetedSelected,this.shipOwnIconTargeted,this.shipOwnIconTargetedSelected,
                ]
            ],
            ["WEAPON", 
                [
                    this.WeaponIconOffline, 
                        //TODO: Create normal Tiny versions
                    this.WeaponIcon,this.WeaponIconSelected,this.WeaponIcon,this.WeaponIconSelected,
                        //TODO: Create no fuel Tiny versions
                    this.WeaponIconNoFuel,this.WeaponIconSelectedNoFuel,this.WeaponIconNoFuel,this.WeaponIconSelectedNoFuel,
                ]
            ]
        ]
    );
    
    
    
    constructor(track: Track) {
        this.track = track;
        //Fake Track, used only to fire deselection events
        if(track.trackId == "-1"){
            return;
        }
        // Create Track marker
        this.marker = new R.RotatedMarker([track.latitude,track.longitude], 
            {icon: this.getIcon(true), rotationAngle: track.courseOverGround - 90});

    // Create Track course history polyline
      let polylinePoints = [];
      polylinePoints.push(new L.LatLng(track.latitude, track.longitude));
      polylinePoints.push(new L.LatLng(track.projetedLatitude, track.projectedLongitude));
      this.lineProjection = L.polyline(polylinePoints, { color: 'red', stroke: true, fill: false, weight: 1, opacity: 0.3 }); 
      
      if(!track.hit){
        let radiusNoise = MapService.getRadiusNoise(track, 0);
        this.noiseMarker =  L.circleMarker([track.latitude,track.longitude], {
          "radius": radiusNoise, "fillColor": "#ff7800", "color": "#ff7800", "weight": 1, "opacity": 0.01}).addTo(MapService.map);        
      }
    
    }

    public getIcon(online: boolean): L.Icon<L.IconOptions> {
        let result = this.getSpecificIcon(online);
        if(typeof result != 'undefined'){
            return result;
        }
        return this.shipOwnIcon;
    }

    private getSpecificIcon(online: boolean): L.Icon<L.IconOptions> | undefined{
        /** From Array Index Definition:
        * General rule: IconIndex = 8*Targeted + 4*Fuel + 2*Tiny + Selected + 1
        */
       let zoomLevel = MapService.map.getZoom();
        let iconMapIndex = 1;
        if(!online){
            return this.iconsMultiMap.get(this.track.sensorTrackType)?.at(0);
        }
        if(this.track.hit) {iconMapIndex+=8;}      
        if(this.track.fuelInGallons == 0) {iconMapIndex+=4;}       
        if(zoomLevel < 19) { iconMapIndex+=2;}
        if(this.selected) {iconMapIndex+=1;}
        return this.iconsMultiMap.get(this.track.sensorTrackType)?.at(iconMapIndex);
    }

    public update(receivedTrack: Track): void {
        // Update Track tactical situation
        this.track = receivedTrack;

        // Update Track marker icon, position and angle
        this.marker.setIcon(this.getIcon(true));
        this.marker.setLatLng(new L.LatLng(this.track.latitude, this.track.longitude));
        this.marker.setRotationAngle(this.track.courseOverGround - 90);
        let currentLatLong = new L.LatLng(this.track.latitude, this.track.longitude);
        let projectedLatLong = new L.LatLng(this.track.projetedLatitude, this.track.projectedLongitude);
        // If the track is the current selection, push the new tactical point to its history
        if(MapService.selectedTrackId[0] == this.track.trackId && MapService.selectedTrackId[1] == "SYSTEMTRACK"){ 
            if(typeof this.lineHistory !== 'undefined' && this.lineHistory != null){
                this.lineHistory.addLatLng([this.track.latitude, this.track.longitude]);
            }
        }
        // Update Track projected course polyline
        let latlongs = this.lineProjection.getLatLngs();
        latlongs.pop(); 
        latlongs.pop(); 
        latlongs.push(currentLatLong); 
        latlongs.push(projectedLatLong);
        this.lineProjection.setLatLngs(latlongs);
        
        // Update the circle markers only if Track is Operational:
        if(!this.track.hit) {
            // TODO: await Track.loadDetections(ownShipTrack);
            this.noiseMarker.setLatLng(new L.LatLng(this.track.latitude, this.track.longitude));
            let radiusNoise = MapService.getRadiusNoise(this.track, 0);
            this.noiseMarker.setRadius(radiusNoise);

            // If specification for this track still not received, cannot start sonar neither radar.
            if(typeof this.track.specification === 'undefined'){
                return;
            } 
            // If Sonar is not Started yet, start it, else, only update it.
            this.updateSonar();
            // 1st Spike for Radar Detection: Only for OWNSHIPS    
            if(this.track.sensorTrackType == "OWNSHIP"){
                this.updateRadar()          
            }
        }
    }

    private updateSonar() {
        //(Need to receive at least one TRACKDAO_PERFORMANCE message)
        if(typeof this.track.specification === 'undefined'){
          return;
        } 
        // If Sonar is not Started yet, start it, else, only update it.
        if(!this.sonarStarted){
            let radiusSonarDetection = MapService.getRadiusSensorDetection(this.track.latitude, this.track.longitude, this.track.specification.sonarDetectionRange);
            let sonarDetectionAzimuthRange = this.track.specification.sonarDetectionAzimuthRange;
            this.detectionMarker = new ArcMarker([this.track.latitude,this.track.longitude], {
              "radius": radiusSonarDetection, "color": "#0099ff", "weight": 2, "opacity": 0.01
            });
            this.detectionMarker.setDirection(this.track.courseOverGround,sonarDetectionAzimuthRange);
            this.detectionMarker.addTo(MapService.map);
            if(this.track.sensorTrackType == "WEAPON"){
                this.amplifiedDetectionMarker = new ArcMarker([this.track.latitude,this.track.longitude], {
                "radius": MapService.WEAPON_AUTOGUIDED_GAIN_AZIMUTH * radiusSonarDetection, "color": "#dd0000", "weight": 2, "opacity": 0.5});
                this.amplifiedDetectionMarker.setDirection(this.track.courseOverGround, MapService.WEAPON_AUTOGUIDED_SENSIBILITY_AZIMUTH);
                this.amplifiedDetectionMarker.addTo(MapService.map);
                // 1st Spike for Weapon with Amplified Searching Animation : TODO: Use Parameters from BackEnd (WEAPON RADAR PARAMETERS)
                this.amplifiedDetectionMarker.counter = 0;
                setInterval( () => {
                    this.amplifiedDetectionMarker.counter += 1;
                    this.amplifiedDetectionMarker
                    .setDirection(this.track.courseOverGround + this.amplifiedDetectionMarker.counter , MapService.WEAPON_AUTOGUIDED_SENSIBILITY_AZIMUTH);
                }, 10); 
            } 
            this.sonarStarted = true;
        }
        else{
            let radiusSonarDetection = MapService.getRadiusSensorDetection(this.track.latitude,this.track.longitude, this.track.specification.sonarDetectionRange);	
            this.detectionMarker.setLatLng(new L.LatLng(this.track.latitude, this.track.longitude));
            this.detectionMarker.setRadius(radiusSonarDetection);
            this.detectionMarker.setDirection(this.track.courseOverGround, this.track.specification.sonarDetectionAzimuthRange);
            if(this.track.sensorTrackType == "WEAPON"){
                this.amplifiedDetectionMarker.setLatLng(new L.LatLng(this.track.latitude, this.track.longitude));
                this.amplifiedDetectionMarker.setRadius(MapService.WEAPON_AUTOGUIDED_GAIN_AZIMUTH * radiusSonarDetection);     
            }
        }
      }


    private updateRadar() {
        //(Need to receive at least one TRACKDAO_PERFORMANCE message)
        if(typeof this.track.specification === 'undefined'){
            return;
        } 
        //If Radar is not started yet, start it
        if(!this.radarStarted){ 
        let radiusRadarDetection = MapService.getRadiusSensorDetection(this.track.latitude, this.track.longitude, this.track.specification.radarDetectionRange);
        this.radarDetectionMarker = new ArcMarker([this.track.latitude,this.track.longitude], {
                  "radius": radiusRadarDetection, "color": "#003c00", "weight": 2, "opacity": 0.05
        });
        let radarDetectionAzimuthRange = this.track.specification.radarDetectionAzimuthRange;
        let radarAntennaTurnSpeed = this.track.specification.radarAntennaTurnSpeed;
        // 1st Spike for RADAR ANTENNA TURNING Animation : TODO: Use Parameters from BackEnd (WEAPON RADAR PARAMETERS)
        this.radarDetectionMarker.counter = 0;
        setInterval( () => {
            this.radarDetectionMarker.counter += 1;
            this.radarDetectionMarker.setDirection(this.track.courseOverGround + this.radarDetectionMarker.counter, radarDetectionAzimuthRange);
        }, Math.round(1000/radarAntennaTurnSpeed));
        this.radarDetectionMarker.addTo(MapService.map);
        this.radarStarted = true;
        }
        //If Radar is already started, update it
        else{
            let radiusRadarDetection = MapService.getRadiusSensorDetection(this.track.latitude, this.track.longitude, this.track.specification.radarDetectionRange);
            this.radarDetectionMarker.setLatLng(new L.LatLng(this.track.latitude, this.track.longitude));
            this.radarDetectionMarker.setRadius(radiusRadarDetection); 
        } 
    }

    public updateRadius(): void {
        if(typeof this.track.specification === 'undefined'){
            return;
        } 
        let radiusNoise = MapService.getRadiusNoise(this.track, 0);
        this.noiseMarker.setRadius(radiusNoise);
        let radiusSonarDetection = MapService.getRadiusSensorDetection(this.track.latitude, this.track.longitude, this.track.specification.sonarDetectionRange);	
        this.detectionMarker.setRadius(radiusSonarDetection);
        // 1st Spike for Radar Detection: Only for OWNSHIPS   
        if(this.track.sensorTrackType == "OWNSHIP"){
            let radiusRadarDetection = MapService.getRadiusSensorDetection(this.track.latitude, this.track.longitude, this.track.specification.radarDetectionRange);	
            this.radarDetectionMarker.setRadius(radiusRadarDetection);
        }
        if(this.track.sensorTrackType == "WEAPON"){
            this.amplifiedDetectionMarker.setRadius(MapService.WEAPON_AUTOGUIDED_GAIN_AZIMUTH * radiusSonarDetection);
        }
    }   
}
