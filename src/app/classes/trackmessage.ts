export class TrackMessage {
    component: string;
    timestamp: number;
    trackId: string;
    message: any;
    topic: string;
    constructor(component:string, timestamp:number, trackId: string, topic:string, message: string){
        this.component = component;
        this.timestamp = timestamp;
        this.trackId = trackId;
        this.message = message;
        this.topic = topic;
    }
}
