import { AISMessage } from "./AISMessage";
import { AISMessageType } from "./types/AISMessageType";

export class ShipAndVoyageData extends AISMessage  {

    imo:IMO;
    callsign:string;
    shipName:string;
    shipType:ShipType;
    toBow:number;
    toStern:number;
    toStarboard:number;
    toPort:number;
    positionFixingDevice:PositionFixingDevice
    etaMonth:number;
    etaDay:number;
    etaHour:number;
    etaMinute:number;
    draught:number;
    destination:string;
    dataTerminalReady:boolean;

    public getMessageType():AISMessageType {
        return AISMessageType.SHIP_AND_VOYAGE_RELATED_DATA;
    }
}

class IMO  {
	imoValue:number;
}