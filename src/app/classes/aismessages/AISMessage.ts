import { AISMessageType } from "./types/AISMessageType";

export abstract class AISMessage {
    // The NMEA messages which represent this AIS message
    nmeaMessage:string[];
    metadata:Metadata;
    // Payload expanded to string of 0's and 1's.
    bitString:string;
    //Length of bitString
    numberOfBits:number;
    repeatIndicator:number;
    sourceMmsi:MMSI;
    nmeaTagBlock:NMEATagBlock;

    messageType:AISMessageType;
    creationTime:number;
}

class Metadata {
    source:string;
    received:number;
}

class MMSI {
    mmsiValue:number;
}

class NMEATagBlock {
    timestamp:number;
    destinationId:string;
    sentenceGrouping:string;
    lineCount:number;
    relativeTime:number;
    sourceId:string;
    text:string;
    checksum:number;
    rawMessage:string;
    valid:boolean;
}