export abstract class ApplicationSpecificMessage {
    binaryData: string;
    designatedAreaCode: number;
    functionalId: number;
}