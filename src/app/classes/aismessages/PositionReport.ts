import { AISMessage } from "./AISMessage";
import { CommunicationState } from "./types/CommunicationState";
import { ManeuverIndicator } from "./types/ManeuverIndicator";
import { NavigationStatus } from "./types/NavigationStatus";

export abstract class PositionReport extends AISMessage {

    navigationStatus:NavigationStatus;
    rateOfTurn:number;
    speedOverGround:number;
    positionAccuracy:boolean;
    latitude:number;
    longitude:number;
    courseOverGround:number;
    trueHeading:number;
    second:number;
    specialManeuverIndicator:ManeuverIndicator;
    raimFlag:boolean;
    communicationState:CommunicationState;
}