import { PositionReport } from "../PositionReport";
import { AISMessageType } from "./AISMessageType";

export class PositionReportClassAScheduled extends PositionReport {

    public getMessageType():AISMessageType {
        return AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED;
    }
}