import * as L from 'leaflet';
import { ArcMarker } from 'leaflet-marker-arc';
import * as R from 'leaflet-marker-rotation';
import { MapService } from '../services/map.service';
import { AISMessageType } from './aismessages/types/AISMessageType';
import { AISTrackInfo } from "./aistrackinfo";

export class AISTrackView {
    aisTrackInfo: AISTrackInfo;
    selected: boolean = false;
    marker: R.RotatedMarker;
    lineHistory: any;

    radarStarted: boolean = false;
    radarDetectionMarker: any
    /** 
     * We dont know the radar range for our AISTracks until be feeded by TrackActorDb, so just to represent the 
     * radar antena rateofTurn attribute we will hardcode this magic number of 400Km with 6 degrees of beam angle.
     * TODO: Provide capability to inform Ship caracteristics through TrackActorDb
     */
    radarDetectionRange = 400000;
    radarDetectionAzimuthRange = 6;

    aisIcon = L.icon({ iconUrl: 'assets/ship-generic.png', iconAnchor: [15, 5]});
    aisIcon_tiny = L.icon({ iconUrl: 'assets/ship-generic_tiny.png', iconAnchor: [1, 1]});
    aisIconOffline = L.icon({ iconUrl: 'assets/ownship-offline.png', iconAnchor: [15, 5]});
    aisIconSelected = L.icon({ iconUrl: 'assets/ship-generic-selected.png', iconAnchor: [15, 5]});
    aisIconSelected_tiny = L.icon({ iconUrl: 'assets/ship-generic_tiny_selected.png', iconAnchor: [1, 1]});

    baseIcon = L.icon({ iconUrl: 'assets/icon-base.png', iconAnchor: [20, 5]});
    baseIcon_tiny = L.icon({ iconUrl: 'assets/icon-base_tiny.png', iconAnchor: [20, 5]});
    baseIconSelected = L.icon({ iconUrl: 'assets/icon-base-selected.png', iconAnchor: [20, 5]});
    baseIconSelected_tiny = L.icon({ iconUrl: 'assets/icon-base-selected_tiny.png', iconAnchor: [20, 5]});

    airplaneIcon = L.icon({ iconUrl: 'assets/icon-airplane.png', iconAnchor: [15, 5]});
    airplaneIcon_tiny = L.icon({ iconUrl: 'assets/icon-airplane_tiny.png', iconAnchor: [1, 1]});
    airplaneIconSelected = L.icon({ iconUrl: 'assets/icon-airplane-selected.png', iconAnchor: [15, 5]});
    airplaneIconSelected_tiny = L.icon({ iconUrl: 'assets/icon-airplane-selected_tiny.png', iconAnchor: [1, 1]});

    /** Array Index Definition:
     * [0] - Icon Offline
     * [1] - (0,0):(Not Tiny, Not Selected) - Icon Normal Size, Online
     * [2] - (0:1):(Not Tiny, Selected) - Icon Normal Size, Online, Selected
     * [3] - (1:0):(Tiny, Not Selected) - Icon Tiny Size, Online
     * [4] - (1:1):(Tiny, Selected) - Icon Tiny Size, Online, Selected
     * General rule: IconIdex = 2*Tiny + Selected + 1
     */
    iconsBasicConf: Array<L.Icon> = [this.aisIconOffline, this.aisIcon,this.aisIconSelected,this.aisIcon_tiny,this.aisIconSelected_tiny];
    iconsMultiMap: Map<AISMessageType, Array<L.Icon>> = new Map<AISMessageType, Array<L.Icon>>(
        [
            [AISMessageType.AID_TO_NAVIGATION_REPORT, this.iconsBasicConf],
            [AISMessageType.BASE_STATION_REPORT, 
                [this.aisIconOffline, this.baseIcon,this.baseIconSelected,this.baseIcon_tiny,this.baseIconSelected_tiny]],
            [AISMessageType.CLASS_BCS_STATIC_DATA_REPORT, this.iconsBasicConf],
            [AISMessageType.EXTENDED_CLASS_B_EQUIPMENT_POSITION_REPORT, this.iconsBasicConf],
            [AISMessageType.LONG_RANGE_BROADCAST_MESSAGE,this.iconsBasicConf],          
            [AISMessageType.POSITION_REPORT_CLASS_A_ASSIGNED_SCHEDULE, this.iconsBasicConf],
            [AISMessageType.POSITION_REPORT_CLASS_A_RESPONSE_TO_INTERROGATION, this.iconsBasicConf],
            [AISMessageType.POSITION_REPORT_CLASS_A_SCHEDULED, this.iconsBasicConf],
            [AISMessageType.STANDARD_CLASS_BCS_POSITION_REPORT,this.iconsBasicConf],
            [AISMessageType.STANDARD_SAR_AIRCRAFT_POSITION_REPORT,
                 [this.aisIconOffline, this.airplaneIcon,this.airplaneIconSelected,this.airplaneIcon_tiny,this.airplaneIconSelected_tiny]]
        ]
    );


    constructor(aisTrackInfo: AISTrackInfo) {
        this.aisTrackInfo = aisTrackInfo;
        let latitude = this.aisTrackInfo.aisTrack.latitude;
        let longitude = this.aisTrackInfo.aisTrack.longitude;
        // No reported positions for the AISTrack, cannot be geo represented on map.
        if(latitude == 0 && longitude == 0 ){
            return;
        }
        // Create Track marker
        let type:string = this.aisTrackInfo.aisTrack.messageType.toString();
        let rotationAngle = this.aisTrackInfo.aisTrack.courseOverGround - 90
        if(type == "BASE_STATION_REPORT"){
            rotationAngle = 0;
        }
        this.marker = new R.RotatedMarker([latitude,longitude], 
            {icon: this.getIcon(true), rotationAngle: rotationAngle});
    }

    public getIcon(online: boolean): L.Icon<L.IconOptions> {
        let result = this.getSpecificIcon(online);
        if(typeof result != 'undefined'){
            return result;
        }
        return this.aisIcon;
    }

    private getSpecificIcon(online: boolean): L.Icon<L.IconOptions> | undefined{
        /** From Array Index Definition:
        * General rule: IconIndex = 2*Tiny + Selected + 1
        */
        let iconMapIndex = 1;
        let zoomLevel = MapService.map.getZoom();
        if(!online){
            return this.iconsMultiMap.get(AISMessageType.AID_TO_NAVIGATION_REPORT)?.at(0);
        }
        if(zoomLevel < 16) { iconMapIndex+=2;}
        if(this.aisTrackInfo.selected) {iconMapIndex+=1;}
        let type: AISMessageType = (<any>AISMessageType)[this.aisTrackInfo.aisTrack.messageType];
        return this.iconsMultiMap.get(type)?.at(iconMapIndex);
    }
    public update(receivedTrack: AISTrackInfo): void {
        // Update Track tactical situation
        this.aisTrackInfo = receivedTrack;
        let aisTrack = this.aisTrackInfo.aisTrack;

        // No reported positions for the AISTrack, cannot be geo represented on map.
        if(aisTrack.latitude == 0 && aisTrack.longitude == 0 ){
            return;
        }
        if(typeof this.marker != 'undefined'){
            // Update Track marker icon, position and angle
            let icon = this.getIcon(true);
            this.marker.setIcon(icon);
            this.marker.setLatLng(new L.LatLng(aisTrack.latitude, aisTrack.longitude));

            let rotationAngle = aisTrack.courseOverGround - 90
            let type:string = this.aisTrackInfo.aisTrack.messageType.toString();
            if(type == "BASE_STATION_REPORT"){
                rotationAngle = 0;
            }
            this.marker.setRotationAngle(rotationAngle);
        }

        if(MapService.selectedTrackId[0] ==  "AIS" && MapService.selectedTrackId[1] == aisTrack.mmsi.toString()){ 
            if(typeof this.lineHistory !== 'undefined' && this.lineHistory != null){
                this.lineHistory.addLatLng([aisTrack.latitude, aisTrack.longitude]);
            }
        }
        /* TODO: Debug/Reimplement Radar 
        // If Radar is not Started yet, start it, else, only update it. 
        if(!this.radarStarted){
            this.startRadar();
        }
        else{
            let radiusRadarDetection = MapService.getRadiusSensorDetection(receivedTrack.aisTrack.latitude, receivedTrack.aisTrack.longitude, this.radarDetectionRange);
            this.radarDetectionMarker.setLatLng(new L.LatLng(aisTrack.latitude, aisTrack.longitude));
            this.radarDetectionMarker.setRadius(radiusRadarDetection); 
        } 
        */         
    }
    public startRadar() {
        let latitude = this.aisTrackInfo.aisTrack.latitude;
        let longitude = this.aisTrackInfo.aisTrack.longitude;
        let radiusRadarDetection = MapService.getRadiusSensorDetection(latitude, longitude, this.radarDetectionRange);
        this.radarDetectionMarker = new ArcMarker([latitude,longitude], {
                  "radius": radiusRadarDetection, "color": "#003c00", "weight": 2, "opacity": 0.05
        });
        let radarDetectionAzimuthRange = this.radarDetectionAzimuthRange;
        let radarAntennaTurnSpeed = this.aisTrackInfo.aisTrack.rateOfTurn;
        this.radarDetectionMarker.counter = 0;
        setInterval( () => {
            this.radarDetectionMarker.counter += 1;
            this.radarDetectionMarker.setDirection(this.aisTrackInfo.aisTrack.courseOverGround + this.radarDetectionMarker.counter, radarDetectionAzimuthRange);
        }, Math.round(1000/radarAntennaTurnSpeed));
        this.radarDetectionMarker.addTo(MapService.map);
        this.radarStarted = true;
    }
    public updateRadius() {
        let latitude = this.aisTrackInfo.aisTrack.latitude;
        let longitude = this.aisTrackInfo.aisTrack.longitude;
        let radiusRadarDetection = MapService.getRadiusSensorDetection(latitude, longitude, this.radarDetectionRange);	
        this.radarDetectionMarker.setLatLng(new L.LatLng(latitude, longitude));
        this.radarDetectionMarker.setRadius(radiusRadarDetection);

    }
}
