import { ApplicationSpecificMessage } from "./aismessages/ApplicationSpecificMessage";
import { AISMessageType } from "./aismessages/types/AISMessageType";
import { CommunicationState } from "./aismessages/types/CommunicationState";
import { ManeuverIndicator } from "./aismessages/types/ManeuverIndicator";
import { NavigationStatus } from "./aismessages/types/NavigationStatus";
import { PositionFixingDevice } from "./aismessages/types/PositionFixingDevice";
import { ShipType } from "./aismessages/types/ShipType";

export class AISTrack {

applicationSpecificMessage: ApplicationSpecificMessage;
binaryData: string;
callsign: string;
communicationState:CommunicationState;
courseOverGround:number;
creationTime: number;
dataTerminalReady: boolean;
designatedAreaCode: number;
destination: string;
destinationMmsi: number; //ATENTION
draught:number;
etaDay: number;
etaHour: number;
etaMinute: number; 
etaMonth: number;
functionalId: number;
id: number;
imo: number ; //ATENTION
latitude:number;
longitude:number;
messageType:AISMessageType;
mmsi: number;
navigationStatus:NavigationStatus;
positionAccuracy:boolean;
positionFixingDevice: PositionFixingDevice;
raimFlag:boolean;
rateOfTurn:number;
repeatIndicator: number;
retransmit: boolean;
second:number;
sequenceNumber: number;
shipName: string;
shipType: ShipType;
spare: number;
specialManeuverIndicator:ManeuverIndicator;
speedOverGround:number;
toBow: number;
toPort: number;
toStarboard: number;
toStern: number;
trueHeading:number;
updateTime: number;
}
