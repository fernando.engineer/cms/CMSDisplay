import { TrackSpecification } from "./trackspecification";

export class Track {
    trackId: string;
    playerId: number;
    sensorTrackType: string;
    name: string;
    orderedSpeedOverGround: number;
    orderedCourseOverGround: number;
    speedOverGround: number;
    courseOverGround: number;
    latitude: number;
    longitude: number;
    projetedLatitude: number;
    projectedLongitude: number;
    distanceTravelled: number;
    fuelInGallons: number;
    dBNoise: number;
    weaponsAvailable: number;
    hit:boolean;
    radarAntennaTurnStatus:boolean;
    radarStatus:boolean;
    radarAzimuth:boolean;
    creationTime: number;
    updatedTime: number;
    specification: TrackSpecification | undefined;
}