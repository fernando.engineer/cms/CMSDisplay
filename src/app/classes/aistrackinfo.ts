import { AISMessage } from "./aismessages/AISMessage";
import { AISTrack } from "./aistrack";

export class AISTrackInfo {
    aisTrack:AISTrack;
    aisTrackMessages:AISMessage[];
    selected:boolean = false;    

    constructor(aisTrack:AISTrack, aisTrackMessages:AISMessage[]){
        this.aisTrack = aisTrack;
        this.aisTrackMessages = aisTrackMessages;
    }

}