export class TrackTacticalUpdate {
    trackId: string;
    speedOverGround: number;
    courseOverGround: number;
    latitude: number;
    longitude: number;
    fuelInLiters: number;
    noiseInDb: number;
    creationTime: number;
}