export class TrackAlert {
    timestamp: number;
    trackId: string;
    message: string;
    checked: boolean;
    constructor(timestamp:number, trackId:string, message: string){
        this.timestamp = timestamp;
        this.trackId = trackId;
        this.message = message;
        this.checked = false;
    }
}
