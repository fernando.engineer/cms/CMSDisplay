import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AISTrackInfo } from '../classes/aistrackinfo';
import { TrackView } from '../classes/track-view';
import { AisService } from '../services/ais.service';
import { AlertsService } from '../services/alerts.service';
import { MapService } from '../services/map.service';
import { TrackService } from '../services/track.service';

@Component({
  selector: 'app-track-list',
  templateUrl: './track-list.component.html',
  styleUrls: ['./track-list.component.css']
})
export class TrackListComponent implements OnInit {
  minimizedList:boolean = true;
  launchShipWindow:boolean=false;
  launchShipForm: FormGroup;
  formBuilder: any;
  interval: any;
  selectedCoordinate: any;


  public tracksViewMap:Map<string,TrackView> = new Map<string, TrackView>();
  public aisTracksInfoMap = new Map<number,AISTrackInfo>();

  constructor(public mapService:MapService, public trackService:TrackService, public aisService:AisService, public alertService:AlertsService, fb: FormBuilder) {

    this.launchShipForm = new FormGroup({
      ownship_name: new FormControl(),
      ownship_sog: new FormControl(),
      ownship_cog: new FormControl()
    });

    this.launchShipForm = fb.group({
      ownship_name: ["", Validators.required],
      ownship_sog: ["", Validators.required],
      ownship_cog: ["", Validators.required],
    });
    this.aisService.aisTracksInfoMap$.subscribe(data => this.aisTracksInfoMap = data);
    this.trackService.tracksViewMap$.subscribe(data => this.tracksViewMap = data);

  }

  ngOnInit(): void {
    this.interval = setInterval(() => { 
      this.selectedCoordinate = this.trackService.selectedCoordinate;
    }, 2000);
  }

  changeInfoView(){
    this.minimizedList = ! this.minimizedList;
  }

  openLaunchShipWindow(){
    this.minimizedList = true;
    this.launchShipWindow = true;
  }
  
  endLaunchingProcess(){
    this.minimizedList = true;
    this.launchShipWindow = false;
  }


  launchShip() {
    var formData: any = new FormData();
    formData.append('ownship_playerId', 1);
    formData.append('ownship_mmsi', 'deprecated');
    formData.append('ownship_name', this.launchShipForm.get('ownship_name')?.value);
    formData.append('ownship_lat', this.selectedCoordinate.lat);
    formData.append('ownship_lng', this.selectedCoordinate.lng);
    formData.append('ownship_sog', this.launchShipForm.get('ownship_sog')?.value);
    formData.append('ownship_cog', this.launchShipForm.get('ownship_cog')?.value);
    this.trackService.postTrackFormData(formData);
    this.endLaunchingProcess()
  }
  
  toDegreesMinutesAndSeconds(coordinate: number){
    var absolute = Math.abs(coordinate);
    var degrees = Math.floor(absolute);
    var minutesNotTruncated = (absolute - degrees) * 60;
    var minutes = Math.floor(minutesNotTruncated);
    var seconds = Math.round(((minutesNotTruncated - minutes) * 60) * 100.0) / 100.0;
    return degrees + "\xB0" + minutes + "'" + seconds + "''";
    }
    
}