import { Component } from '@angular/core';
import { AISMessage } from '../classes/aismessages/AISMessage';
import { AISTrackInfo } from '../classes/aistrackinfo';
import { AisService } from '../services/ais.service';
import { MapService } from '../services/map.service';
import { TrackService } from '../services/track.service';

@Component({
  selector: 'app-trackinfo',
  templateUrl: './trackinfo.component.html',
  styleUrls: ['./trackinfo.component.css']
})
export class TrackinfoComponent {
  selectedTrack:any = null;
  selectedAISTrackInfo:AISTrackInfo | null  = null;
  interval: any;
  displaySpecification:boolean=false;
  displayInfo:boolean=true;
  displayAISMessagesListInfo:boolean=false;
  displayAISMessageSelected:boolean=true;
  aisMessageSelected:AISMessage | null = null;
  deltaCoGValue:number=1;
  deltaSoGValue:number=1;

  constructor(public trackService:TrackService, public aisService:AisService, public mapService: MapService) { 
    this.trackService.selectedTrack$.subscribe(data => { 
      data.track.trackId != '-1' ? this.selectedTrack = data.track : this.selectedTrack = null;
    });
    this.aisService.aisTracksInfoSelected$.subscribe(data => { 
      data.selected ? this.selectedAISTrackInfo = data : this.selectedAISTrackInfo = null;
      this.aisMessageSelected = null;
    });
  }

  formatLatitude(coordinate: number | undefined){
    if(typeof coordinate == 'undefined'){
      return 'Undefined'
    }
    let direction:string = coordinate >= 0 ? "N" : "S"
    return this.toDegreesMinutesAndSeconds(coordinate) + direction;
  }

  formatLongitude(coordinate: number | undefined){
    if(typeof coordinate == 'undefined'){
      return 'Undefined'
    }
    let direction:string = coordinate >= 0 ? "E" : "W"
    return this.toDegreesMinutesAndSeconds(coordinate) + direction;
  }


  toDegreesMinutesAndSeconds(coordinate: number){
    var absolute = Math.abs(coordinate);
    var degrees = Math.floor(absolute);
    var minutesNotTruncated = (absolute - degrees) * 60;
    var minutes = Math.floor(minutesNotTruncated);
    var seconds = Math.round(((minutesNotTruncated - minutes) * 60) * 100.0) / 100.0;
    return degrees + "\xB0" + minutes + "'" + seconds + "''";
  }

  toformattedReceivedDate(timeInMillis:number | undefined){
    if(typeof timeInMillis == 'undefined' || !Number.isInteger(timeInMillis)){
      return 'Date Not provided.'
    }
    var options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' , hour12: false} as const;
    const enUSFormatter = new Intl.DateTimeFormat('en-US',options );
    return enUSFormatter.format(new Date(timeInMillis));
}

  launchShip(){
    console.log("TODO: launchShip");
  }

  deleteTrack(trackId:string){
  this.trackService.deleteTrack(trackId);
  this.displaySpecification=false;
  this.displayInfo=false;
  }

  launchWeapon(trackId:string){
  this.trackService.launchWeapon(trackId);
  }

  changeInfo(){
    this.displayInfo = ! this.displayInfo;
  }

  changeInfoSpecification(){
    this.displaySpecification = ! this.displaySpecification;
  }

  changeInfoAISMessagesList(){
    this.displayAISMessagesListInfo = ! this.displayAISMessagesListInfo;
  }

  showAISMessageSelected(msg:AISMessage){
    this.aisMessageSelected = msg;
  }


  orderCoG(positiveDelta:boolean){
    let deltaCoG:number;  
    positiveDelta ? deltaCoG = this.deltaCoGValue : deltaCoG = -1 * this.deltaCoGValue;
    let orderCoG = this.selectedTrack.orderedCourseOverGround + deltaCoG;
    if(orderCoG > 360){ orderCoG -= 360; }
    if(orderCoG < 0){ orderCoG += 360; }
    orderCoG *= 100;
    this.trackService.orderCoG(this.selectedTrack.trackId,Math.round(orderCoG));
  }

  orderSoG(positiveDelta:boolean){
    let deltaSoG:number;  
    positiveDelta ? deltaSoG = this.deltaSoGValue : deltaSoG = -1 * this.deltaSoGValue;
    let orderSoG = this.selectedTrack.orderedSpeedOverGround + deltaSoG;
    orderSoG *= 100;
    this.trackService.orderSoG(this.selectedTrack.trackId,Math.round(orderSoG));
  }


}
