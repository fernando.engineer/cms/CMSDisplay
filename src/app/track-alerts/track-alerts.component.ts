import { Component } from '@angular/core';
import { AlertsService } from '../services/alerts.service';
import { TrackService } from '../services/track.service';

@Component({
  selector: 'app-track-alerts',
  templateUrl: './track-alerts.component.html',
  styleUrls: ['./track-alerts.component.css']
})
export class TrackAlertsComponent {
  minimizedList:boolean = false;

  constructor(public alertService: AlertsService,  public trackService:TrackService) { }

  changeAlertsView(){
    this.minimizedList = ! this.minimizedList;
  }

}
