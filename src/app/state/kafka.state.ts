import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Track } from '../classes/track';
import { AddMessageAction } from './kafka.actions';
import { TrackSpecification } from '../classes/trackspecification';
import { TrackMessage } from '../classes/trackmessage';

export class KafkaStateModel {
  public components: string[];
  public timestamps: number[];
  public topics: string[];
  public trackIds: string[];
  public messages: string[];
}

@State<KafkaStateModel>({
  name: 'kafka',
  defaults: {
    components: [],
    timestamps: [],
    topics: [],
    trackIds: [],
    messages: []
  }
})

@Injectable()
export class KafkaState {


  static receivedTracksMap:Map<string,Track> = new Map<string, Track>();
  static trackSpecificationMap: Map<string,TrackSpecification> = new Map<string,TrackSpecification>();

@Selector([KafkaState])
static receiveMessages(state: KafkaStateModel): TrackMessage[]  {
  let trackMessages = [];
  let lastMsgIdx = state.messages.length - 1;
  trackMessages.push(
      new TrackMessage(
        state.components[lastMsgIdx],
        state.timestamps[lastMsgIdx],
        state.trackIds[lastMsgIdx],
        state.topics[lastMsgIdx],
        state.messages[lastMsgIdx] 
      )
    )
  return trackMessages ;
}
  
  @Action(AddMessageAction)
  add(ctxAlert: StateContext<KafkaStateModel>, action: AddMessageAction) {
      let state = ctxAlert.getState();
      ctxAlert.setState({ 
        components: [ ...state.components, action.component ],
        timestamps: [...state.timestamps, action.timestamp],
        topics: [...state.topics, action.topic],
        trackIds: [...state.trackIds, action.trackId],
        messages: [ ...state.messages, action.message ]
      });
    }

}
