
export class AddMessageAction {
  static readonly type = 'CMS_KAFKA_MESSAGE';
  timestamp:number;
  component:string;
  topic:string;
  trackId: string;
  message:string;
  constructor(public payload: string) { 
  }
}
