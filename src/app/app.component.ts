import { Component } from '@angular/core';
import { environment } from './../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CMSDisplay';
  mainView:boolean = true;
  online:boolean=true;
  constructor() {
    console.log(this.title + " started in scope " + environment.scope); // Logs false for development environment
  }
}
